% Download

<script type="text/javascript" language="javascript">
document.getElementById("tab-download").className = "current";
</script>

The latest release of Distem is **version 1.5**.

## Debian packages

### For Debian stretch

~~~~
deb http://packages.grid5000.fr/deb/distem/stretch/ ./
~~~~

### For Debian buster

~~~~
deb http://packages.grid5000.fr/deb/distem/buster/ ./
~~~~

On [Grid'5000](http://www.grid5000.fr/), it is recommended that you use **distem-bootstrap** as explained in the [tutorial](tuto_getting_started.html).

## Source code

The current *git* version can be retrieved with

~~~~ {#mycode .bash}
$> git clone https://gitlab.inria.fr/distem/distem.git
~~~~
