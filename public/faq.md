% Frequently Asked Questions

<script type="text/javascript" language="javascript">
document.getElementById("tab-faq").className = "current";
</script>

## How Distem deals with SSH ?

For each virtual node, Distem:

* completes (or creates if required) /root/.ssh/authorized_keys with
    * the keys in /root/.ssh/authorized_keys from the host
    * the keys in /root/.ssh/*.pub from the host
* copies /root/.ssh/id* from host in /root/.ssh (only if the target files does not exist
* adds an entry in /root/.ssh/authorized_keys if a public key is passed in parameter
* adds (and eventually overwrites) a private key /root/.ssh/identity if a private key is passed in parameter

## How to assign automatically virtual nodes to physical nodes ?

When starting a virtual node (going from state READY to RUNNING), a physical node (that have enough physical resources (CPU,...)) will be automatically allocated if there is none host set.

## What is required to start a virtual node ?

A filesystem image must have been set. The filesystem will be copied on the host physical node.

## What Distem does when it stops a virtual nodes ?

When stopping a virtual node (going from state RUNNING to READY), Distem deletes its data from the hosting physical node. The host association for this virtual node will be canceled. If you start the virtual node directly after stopping it, the hosting physical node will be chosen randomly if it is not specified explicitly.

## How to customize your virtual environment ?

First of all, you have to allow connections to the WWW with (from a virtual node):

~~~~ {#mycode .bash}
    root> ifconfig if0 10.144.0.1 netmask 255.252.0.0
    root> route add default gw 10.147.255.254 dev if0
~~~~

The gateway must be modified according the Grid'5000 site used (cf. the [Grid'5000 dedicated page](https://www.grid5000.fr/mediawiki/index.php/Network_interlink#Virtual_subnets)).

Then, you can install new packages:

~~~~ {#mycode .bash}
    root> apt-get update && apt-get install new_package
~~~~

Finally, you can dump the new image (from the coordinator node):

~~~~ {#mycode .bash}
    root> distem --get-image vnode=vnode_name,dest=OUTPUT_DIRECTORY
~~~~

## What if I have a Docker image ?

For the moment we do not support Docker images, we rely fully on LXC. We plan to support Docker in the near future.
Meanwhile, you can convert your Docker image into LXC images using the following procedure.

Let's suppose we have the following Dockerfile to build an Apache container.

~~~~
     FROM debian:jessie
     RUN apt-get -y update && apt-get install -y apache2
~~~~


This Apache container is built on top of a Debian jessie. Therefore, what we need first is a fresh Debian jessie LXC image that we can build using LXC tools:

~~~~ {#mycode .bash}
    root> lxc-create -n debian_image -t debian -- -r jessie
~~~~

Then, we are going to fetch and generate a tarball with the contents of the `rootfs`:

~~~~ {#mycode .bash}
    root> tar -pczf debian-jessie-lxc.tar.gz -C  /var/lib/lxc/debian_image/rootfs/ .
~~~~

After that, we load this compressed tarball into Docker:

~~~~ {#mycode .bash}
    root> cat debian-jessie-lxc.tar.gz | docker import - debian:jessie
~~~~

We build the image using Docker:

~~~~ {#mycode .bash}
    root> docker build -t apache .
~~~~

Once the image has been built, we run a container in backgroud using the image:

~~~~ {#mycode .bash}
    root> docker run -t -d apache bash
~~~~

And we export its filesystem using its commit ID:

~~~~ {#mycode .bash}
    root> docker export 4e965f3bfb6e > apache-docker-lxc.tar
~~~~

Finally, we compress the image:

~~~~ {#mycode .bash}
    root> gzip apache-docker-lxc.tar
~~~~

The image you have just created can be used on Distem.
However, for some images you should perform some tweaks before. You probably have noticed that
normally there is the keyword `CMD` at the end of Dockerfiles. This is for launching the application. So for LXC images this
has to be replace by a systemd or SysV service script or some hack using the command `nohup`.
