% Documentation

<script type="text/javascript" language="javascript">
document.getElementById("tab-documentation").className = "current";
</script>

<div align="center">
<div class="blocklist" style="width: 500px;  background-color: #dadada; cursor: default;" onclick="location.href='#'">
  <h2>Command line client (man pages)</h2>
  <ul style="text-align: left;">
  <li><a href="man/distem.html">distem</a>: the client tool</li>
  <li><a href="man/distemd.html">distemd</a>: the Distem daemon, that runs on each physical node</li>
  <li><a href="man/distem-bootstrap.html">distem-bootstrap</a>: script to setup a Distem environment</li>
  <li><a href="man/distem-devbootstrap.html">distem-devbootstrap</a>: script to setup a Distem development environment</li>
  </ul>
</div>
<div class="blocklist" style="width: 500px;  background-color: #dadaff;" onclick="location.href='doc/distem-rest.html'">
  <h2><a href="doc/distem-rest.html" style="text-decoration: none;">REST API</a></h2>
  <p>The documentation of the REST API you should use if you want to set up your experimentation platform contacting the Distem coordinator server using REST.</p>
</div>
<div class="blocklist" style="width: 500px; background-color: #ffdada;" onclick="location.href='doc/distem-client.html'">
  <h2><a href="doc/distem-client.html" style="text-decoration: none;">Ruby client library</a></h2>
  <p>The documentation of the ruby client you should use if you want to set up your experimentation platform using a ruby script file.</p>
</div>
<div class="br"></div>
<p>Note for developers: the documentation of Distem core files is available <a href="doc/index.html">here</a></p>
</div>
