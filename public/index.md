% DISTributed systems EMulator

<script type="text/javascript" language="javascript">
document.getElementById("tab-index").className = "current";
</script>


**Distem** is a distributed systems emulator. When doing research on Cloud, High Performance Computing, or Edge system, it can be used to transform an homogenenous cluster (composed of identical nodes) into an experimental platform where nodes have different performance, and are linked together through a complex network topology, making it the ideal tool to benchmark applications targetting such environments.

Key features:

* Uses modern Linux technology to steal resources from your applications
* Easy to install: on Grid'5000, you only need a few minutes to start to use Distem
* Easy to use: simple command-line interface for beginners, REST API for more experienced users
* Efficient and scalable: start a 15000-nodes virtual topology in less than 10 minutes

The current version of Distem is version 1.5, released on 2020-09-08. Distem is released under GPLv3 and the source can be found on the [GitLab project page](https://gitlab.inria.fr/distem/distem.git).

To start using Distem, follow the [tutorial](tuto_getting_started.html).

<div style="margin-left: auto; margin-right: auto; width: 600px">
<iframe src="https://www.slideshare.net/slideshow/embed_code/36993567" width="597" height="486" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>
<div style="text-align: right;">
(<a href="distem.pdf">download as PDF</a>)
</div>
</div>


## Mailing list & contact information
Distem is developed by the [RESIST team](https://team.inria.fr/resist/) at [LORIA](http://www.loria.fr/) and [INRIA Nancy - Grand Est](http://www.inria.fr/nancy/), mainly by Alexandre Merlin, [Emmanuel Jeanvoine](http://www.loria.fr/~ejeanvoi/), [Luc Sarzyniec](http://olbat.net/) and [Lucas Nussbaum](http://www.loria.fr/~lnussbau/). While it is a full rewrite, Distem is built on the foundations of [Wrekavoc](http://wrekavoc.gforge.inria.fr/).

To report issues, use the [GitLab project page](https://gitlab.inria.fr/distem/distem/-/issues).
You can also contact Lucas Nussbaum at [lucas.nussbaum@loria.fr](mailto:lucas.nussbaum@loria.fr).

## Reference publication
* Luc Sarzyniec, Tomasz Buchert, Emmanuel Jeanvoine and Lucas Nussbaum.
 [Design and Evaluation of a Virtual Experimental Environment for Distributed Systems](http://hal.inria.fr/hal-00724308)</br>
 [21st Euromicro International Conference on Parallel, Distributed and Network-Based Processing (PDP 2013)](http://www.pdp2013.org/)</br>
 [paper](http://hal.inria.fr/docs/00/75/74/37/PDF/distem-pdp2013.pdf)

## Publications on Distem internals
* Tomasz Buchert, Lucas Nussbaum and Jens Gustedt.
 [Methods for Emulation of Multi-Core CPU Performance](http://hal.archives-ouvertes.fr/inria-00535534/en/)</br>
 [13th IEEE International Conference on High Performance Computing and Communications (HPCC-2011)](http://cse.stfx.ca/~hpcc2011/)</br>
 [paper](http://hal.inria.fr/docs/00/62/61/61/PDF/hpcc-emul-multicore-cpu-perf.pdf) - [slides](http://hal.inria.fr/docs/00/62/61/61/ANNEX/hpcc-emul-multicore-cpu-perf-slides.pdf)
* Tomasz Buchert, Lucas Nussbaum and Jens Gustedt.
 [Accurate emulation of CPU performance](http://hal.archives-ouvertes.fr/inria-00490108/en)</br>
 [8th International Workshop on Algorithms, Models and Tools for Parallel Computing on Heterogeneous Platforms (HeteroPar'2010)](http://heteropar2010.ens-lyon.fr/)</br>
 [paper](http://hal.inria.fr/docs/00/62/61/64/PDF/heteropar-accurate-emul-cpu.pdf) - [slides](http://hal.inria.fr/docs/00/62/61/64/ANNEX/heteropar-accurate-emul-cpu-slides.pdf)

## Publications using Distem
* Trong-Tuan Vu and Bilel Derbel.
 [Link-Heterogeneous Work Stealing](http://hal.inria.fr/hal-00938835/en)</br>
 CCGrid 2014: 14th IEEE/ACM International Symposium on Cluster, Cloud, and Grid Computing (2014)
* Maximiliano Geier, Lucas Nussbaum and Martin Quinson.
 [On the Convergence of Experimental Methodologies for Distributed Systems: Where do we stand?](http://hal.inria.fr/hal-00907887/en/)</br>
 WATERS - 4th International Workshop on Analysis Tools and Methodologies for Embedded and Real-time Systems (2013)</br>
 [paper](http://hal.inria.fr/docs/00/90/78/87/PDF/waters.pdf)
